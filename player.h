#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>

typedef struct Player {
	char *first_name;
	char *last_name;
	char *major;
	char sex;
	double money;
	float gpa;
	int age;
	int arrests;
	int height;
	int sport;
	int weight;
} player_t;

/* Ensure the enum and sport_t maintain the same order */
typedef enum {FOOTBALL, BASEBALL, BASKETBALL, SOCCER} sport_t;

char *sports[4] = {
	"Football",
	"Baseball",
	"Basketball",
	"Soccer"
};

void input_age(player_t *player);
void input_arrests(player_t *player);
void input_gpa(player_t *player);
void input_height(player_t *player);
void input_major(player_t *player);
void input_money(player_t *player);
void input_name(player_t *player);
void input_sex(player_t *player);
void input_sport(player_t *player);
void input_weight(player_t *player);

void clear_data(player_t *player);
void display_player(player_t *player);
void print_menu(void);
void read_input(player_t *player);
