/* Michael Smith <mjs3@clemson.edu>
 * ECE 2220 - Lab 2
 *
 */

#include "player.h"

/* Direct control functions */
void clear_data(player_t *player) {
	player->first_name = "";
	player->last_name = "";
	player->major = "";
	player->sex = ' ';
	player->money = 0;
	player->gpa = 0;
	player->age = 0;
	player->arrests = 0;
	player->height = 0;
	player->sport = -1;
	player->weight = 0;
}

/* Input functions */
void read_input(player_t *p) {
	int input;
	char *line;

	while ((line = readline("Please select an option from the menu: "))) {
		sscanf(line, "%d", &input);

		switch (input) {
			case 1:
				input_name(p);
				break;
			case 2:
				input_sex(p);
				break;
			case 3:
				input_height(p);
				break;
			case 4:
				input_arrests(p);
				break;
			case 5:
				input_money(p);
				break;
			case 6:
				input_major(p);
				break;
			case 7:
				input_age(p);
				break;
			case 8:
				input_weight(p);
				break;
			case 9:
				input_sport(p);
				break;
			case 10:
				input_gpa(p);
				break;
			case 11:
				display_player(p);
				break;
			case 12:
				clear_data(p);
				display_player(p);
				break;
			case 13:
				return;
				break;
			default:
				break;
		}

		input = 0;
		print_menu();
		free(line);
	}
}

void input_age(player_t *player) {
	int age;
	char *line;

	do {
		line = readline("Enter the player's age: ");
		sscanf(line, "%d", &age);
	} while (age < 17 || age > 99);

	player->age = age;
	free(line);
}

void input_arrests(player_t *player) {
	int arrests;
	char *line;

	do {
		line = readline("Enter number of arrests between 0, and 2 billion: ");
		sscanf(line, "%d", &arrests);
	} while (arrests < 0 || arrests > 2e9);

	player->arrests = arrests;
	free(line);
}

void input_gpa(player_t *player) {
	float gpa;
	char *line;

	do {
		line = readline("Enter player's GPA (0.0 - 4.0): ");
		sscanf(line, "%f", &gpa);
	} while (gpa < 0 || gpa > 4);

	player->gpa = gpa;
	free(line);
}

void input_height(player_t *player) {
	int feet, inches;
	char *line;

	do {
		line = readline("Enter the player's height in feet and inches (4'0\" - 7'11\"): ");
		sscanf(line, "%d'%d\"", &feet, &inches);
	} while ((feet < 4 || feet > 7) && (inches < 0 || inches > 11));

	player->height = 12 * feet + inches;
	free(line);
}

void input_major(player_t *player) {
	char *major;
	char *line;

	line = readline("Enter the player's major: ");
	major = strdup(line);

	player->major = major;
	free(line);
}

void input_money(player_t *player) {
	double money;
	char *line;

	do {
		line = readline("Enter a number between 0 and 1x10^100: ");
		sscanf(line, "%lf", &money);
	} while (money < 0 || money > (10e100));

	player->money = money;
	free(line);
}

void input_name(player_t *player) {
	char *first_name = malloc(32 * sizeof(char));
	char *last_name = malloc(32 * sizeof(char));
	char *line;

	line = readline("Enter the player's name: ");
	sscanf(line, "%s %s\n", first_name, last_name);

	player->first_name = first_name;
	player->last_name = last_name;
	free(line);
}

void input_sex(player_t *player) {
	char sex = ' ';
	char *line;

	do {
		line = readline("Enter the player's sex (M/F): ");
		sscanf(line, "%c", &sex);
		sex = toupper(sex);
	} while (sex != 'M' && sex != 'F');

	player->sex = sex;
	free(line);
}

void input_sport(player_t *player) {
	int i, sport;
	char *line;

	do {
		for (i = 0; i < sizeof(sport_t); i++) {
			printf("%s = %d\n", sports[i], i);
		}
		line = readline("Select the player's sport: ");
		sscanf(line, "%d", &sport);
	} while (sport < 0 || sport > sizeof(sport_t) - 1);

	player->sport = sport;
	free(line);
}

void input_weight(player_t *player) {
	int weight;
	char *line;

	do {
		line = readline("Enter the player's weight (in lbs.): ");
		sscanf(line, "%d", &weight);
	} while (weight < 1 || weight > 1000);

	player->weight = weight;
	free(line);
}

/* Display functions */
void display_player(player_t *player) {
	printf("Player name: %s %s\n"
			"Sex: %c\n"
			"Height: %d\'%d\"\n"
			"Arrests: %d\n"
			"Money paid: $%.2lf\n"
			"Major:	%s\n"
			"Age: %d\n"
			"Weight: %d\n"
			"Sport: %s\n"
			"GPA: %.3f\n",
			player->first_name, player->last_name, player->sex,
			player->height / 12, player->height % 12, player->arrests,
			player->money, player->major, player->age, player->weight,
			sports[player->sport], player->gpa);
}

void print_menu(void) {
	printf( " 1. Enter name\n"
			" 2. Enter sex\n"
			" 3. Enter height\n"
			" 4. Enter number of arrests\n"
			" 5. Enter money paid\n"
			" 6. Enter major\n"
			" 7. Enter age\n"
			" 8. Enter weight\n"
			" 9. Enter sport\n"
			"10. Enter GPA\n"
			"11. Display all data\n"
			"12. Clear all data\n"
			"13. Quit\n");
}

int main(int argc, char **argv) {
	player_t player;
	clear_data(&player);
	print_menu();
	read_input(&player);

	return EXIT_SUCCESS;
}
